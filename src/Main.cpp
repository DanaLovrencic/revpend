#include "Window.hpp"
#include "Errors.hpp"
#include "Renderer.hpp"
#include "log/Logging.hpp"
#include "System.hpp"
#include "ControlSystem.hpp"

#include <iostream>

int main()
{
    try {
        Window w(700, 350, "Reverse pendulum", true);

        System system;

        std::unique_ptr<ControlSystem> control;
        if (system.get_type() == 0) {
            control = std::make_unique<KeyboardControl>(w, system.get_force());
        } else {
            control =
                std::make_unique<GradientDescent>(system, system.get_force());
        }

        while (w.open()) {  // Loop until window is open.
            system.clear(); // Clear the screen.

            // Get acceleration value from keyboard callback or gradient
            // descent function for calculating force.
            float force = control->calculate_force();
            system.calculate_new_state(force);
            system.draw();

            w.swap_buffers(); // Show new frame.
            w.poll_events();  // Poll and process mouse and keyboard events.
        }
    } catch (const std::exception& exc) {
        Log::report_error(exc);
        return 1;
    }

    return 0;
}
