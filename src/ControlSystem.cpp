#include "ControlSystem.hpp"

namespace {
void keyboard_callback(
    GLFWwindow* window, int key, int scancode, int action, int mods);

enum class Direction { left, right, none };
Direction direction;
} // namespace

KeyboardControl::KeyboardControl(const Window& w, float force_value)
    : force_value(force_value)
{
    w.set_keyboard_callback(keyboard_callback);

    direction = Direction::none;
}

float KeyboardControl::calculate_force()
{
    switch (direction) {
        case Direction::none: return 0.0;
        case Direction::right: return force_value;
        case Direction::left: return -force_value;
        default: throw std::logic_error("Unknown direction.");
    }
}

GradientDescent::GradientDescent(System& system, float max_force)
    : system(system), max_force(max_force)
{}

float GradientDescent::calculate_force()
{
    System::State s = system.get_state();
    Solver solver   = system.get_solver();

    float step  = 10;
    float force = 0;

    // For positive angle, force direction is positive.
    int sign = s.angle > 0 ? 1 : -1;

    while (step >= 0.05 && std::abs(force) < max_force) {
        std::array<float, 4> new_state = solver.calculate_new_state(
            {s.angle, s.angular_vel, s.x_cart, s.x_cart_vel}, force);
        float& angle = new_state[0];

        // Check if angle sign changed.
        if ((sign == -1 && angle > 0) || (sign == 1 && angle < 0)) {
            force -= sign * step;
            step /= 5.0f;
        }
        force += sign * step;
    }
    return force;
}

namespace {
void keyboard_callback(GLFWwindow* window, int key, int, int action, int)
{
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_RIGHT: direction = Direction::right; break;
            case GLFW_KEY_LEFT: direction = Direction::left; break;
        }
    } else if (action == GLFW_RELEASE) {
        switch (key) {
            case GLFW_KEY_RIGHT:
                if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
                    direction = Direction::left;
                } else {
                    direction = Direction::none;
                }
                break;
            case GLFW_KEY_LEFT:
                if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
                    direction = Direction::right;
                } else {
                    direction = Direction::none;
                }
                break;
        }
    }
}
} // namespace

