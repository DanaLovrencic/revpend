#include "Cart.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <array>

Cart::Cart(const glm::mat4& proj, const glm::mat4& view, ShaderProgram& program)
    : RenderObject(program, proj, view, RenderPrimitive::TRIANGLE)
{
    VertexBufferLayout layout;
    layout.push<float>(2); // Vertex buffer has 2 integers.

    std::array<glm::vec2, 4> pos = {glm::vec2(-1.0f, -0.25f),
                                    glm::vec2(1.0f, -0.25f),
                                    glm::vec2(1.0f, 0.25f),
                                    glm::vec2(-1.0f, 0.25f)};

    std::array<unsigned, 6> ind = {0, 1, 2, 2, 3, 0};

    ib = std::make_unique<IndexBuffer>(&ind[0], ind.size());

    vb =
        std::make_unique<VertexBuffer>(&pos[0], pos.size() * sizeof(glm::vec2));

    va.add_buffer(*vb, layout);

    set_position(0.0f); // Set initial cart position.

    program.bind();
    program.set_uniform3f("color", glm::vec3(1.0f, 1.0f, 1.0f));
}

void Cart::set_position(float x_cart)
{
    // Calculate new model matrix based on new cart position.
    model =
        glm::translate(glm::mat4(1.0f), glm::vec3(x_cart, -0.2f * 0.25f, 0.0f));
    model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
}
