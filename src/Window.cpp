#include "Window.hpp"

#include <stdexcept>
#include <iostream>

#ifndef NDEBUG
    #include "Errors.hpp"
#endif

Window::Window(unsigned width,
               unsigned height,
               const std::string& title,
               bool enable_vsync)
{
    if (!glfwInit()) throw std::runtime_error("Failed to initialize GLFW.");

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); // Make window fixed size.
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Use core.

    // Create a windowed mode window of provided size.
    window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    if (!window) throw std::runtime_error("Failed to create the window.");

    glfwMakeContextCurrent(window); // Enable OpenGL drawing to this window.

    // Enable or disable VSync
    enable_vsync ? glfwSwapInterval(1) : glfwSwapInterval(0);

    // Initialize modern OpenGL - pull functions from GPU drivers.
    if (glewInit() != GLEW_OK) {
        throw std::runtime_error("Failed to initialize GLEW.");
    }

#ifndef NDEBUG
    Errors::enable(); // Enable OpenGL error reporting.
#endif
}

Window::~Window()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

bool Window::open() const
{
    return !glfwWindowShouldClose(window);
}

void Window::swap_buffers() const
{
    glfwSwapBuffers(window); // Swap front and back buffer.
}

void Window::poll_events() const
{
    glfwPollEvents(); // Poll for events and process them.
}

void Window::set_mouse_button_callback(GLFWmousebuttonfun callback) const
{
    glfwSetMouseButtonCallback(window, callback);
}

void Window::set_keyboard_callback(GLFWkeyfun callback) const
{
    glfwSetKeyCallback(window, callback);
}

std::pair<unsigned, unsigned> Window::dimensions() const
{
    int width, height;
    glfwGetWindowSize(window, &width, &height);
    return {width, height};
}
