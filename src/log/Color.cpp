#include "log/Color.hpp"


namespace Log {

Log::ColorModifier::ColorModifier(Log::ColorCode cc)
    : cc(cc) {}


std::ostream& operator<<(std::ostream& s, const ColorModifier& m)
{
    if (!ColorModifier::enable_colors) return s;
    return s << "\033[" << m.cc << "m";
}

}
