#ifndef SHADER_HPP
#define SHADER_HPP

#include <string>
#include <memory>
#include <unordered_map>

#include <glm/glm.hpp>

class Shader {
  private:
    unsigned shader_id;
    std::string code;
    const unsigned type;

  public:
    Shader(unsigned type, const std::string& shader_filename);
    ~Shader();

    void compile();
    void attach(unsigned program_id) const;
    void detach(unsigned program_id) const;
};

class ShaderProgram {
  private:
    const unsigned program_id;
    std::unordered_map<std::string, unsigned> uniform_location_cache;

  public:
    ShaderProgram(const std::string& vertex_shader_filename,
                  const std::string& fragment_shader_filename);
    ~ShaderProgram();

    void bind() const;
    void unbind() const;

    void set_uniform4f(const std::string& name, const glm::vec4& value);
    void set_uniform3f(const std::string& name, const glm::vec3& value);
    void set_uniform1i(const std::string& name, int value);
    void set_uniform_mat4f(const std::string& name, const glm::mat4& value);

  private:
    int get_uninform_location(const std::string& name);
};

#endif
