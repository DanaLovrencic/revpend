#ifndef COLOR_HPP
#define COLOR_HPP

#include <ostream>

namespace Log {

enum ColorCode {
    FG_RED     = 31,
    FG_GREEN   = 32,
    FG_YELLOW  = 33,
    FG_BLUE    = 34,
    FG_PINK    = 35,
    FG_CYAN    = 36,
    FG_WHITE   = 37,
    FG_DEFAULT = 39,
};

class ColorModifier {
private:
    ColorCode cc;
public:
    ColorModifier(ColorCode cc);

    friend std::ostream& operator<<(std::ostream& s, const ColorModifier& m);

private:
    static constexpr bool enable_colors = true;
};

}

#endif
