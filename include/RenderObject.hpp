#ifndef RENDER_OBJECT_HPP
#define RENDER_OBJECT_HPP

#include "VertexArray.hpp"
#include "Shader.hpp"
#include "IndexBuffer.hpp"
#include "Renderer.hpp"

class RenderObject {
  private:
    RenderPrimitive render_primitive;

  protected:
    VertexArray va;
    ShaderProgram& program;
    const glm::mat4& proj;
    const glm::mat4& view;
    glm::mat4 model;
    std::unique_ptr<IndexBuffer> ib;
    std::unique_ptr<VertexBuffer> vb;

  public:
    RenderObject(ShaderProgram& program,
                 const glm::mat4& proj,
                 const glm::mat4& view,
                 RenderPrimitive render_primitive);
    virtual void draw(Renderer& renderer);
};

#endif
